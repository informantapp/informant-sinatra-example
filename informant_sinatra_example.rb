require 'bundler/setup'

Bundler.require

class User
  include ActiveModel::Model
  include InformantCommon::ValidationTracking

  attr_accessor :name

  validates_presence_of :name
end

class InformantSinatraExample < Sinatra::Base
  register InformantSinatra::Bootstrap

  get '/' do
    erb "
    <h2>Informant Sinatra Example</h2>

    <form method='POST' action='/users'>
      <input type='hidden' name='name' value='Valid Name' />
      <input type='submit' value='Test Successful Validation' />
    </form>

    <br/><br/>

    <form method='POST' action='/users'>
      <input type='hidden' name='name' value='' />
      <input type='submit' value='Test Unsuccessful Validation' />
    </form>
  ", locals: { message: session[:message] }
  end

  post '/users' do
    if User.new(params).valid?
      erb "
      <h2>Logged Successful Validation</h2>

      <p>
        Sign in to
        <a href='https://console.informantapp.com'>the Informant</a>
        and you should see a successful submission on your dashboard.
      </p>

      <p><a href='/'>Go Back</a></p>
    "
    else
      erb "
      <h2>Logged Unsuccessful Validation</h2>

      <p>
        Sign in to
        <a href='https://console.informantapp.com'>the Informant</a>
        and you should see the error on your dashboard.
      </p>

      <p><a href='/'>Go Back</a></p>
    "
    end
  end
end
